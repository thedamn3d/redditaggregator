import os
import unittest
from app import AggregatorContext, CommentEntryBuilder, DataAggregationEngine, KeywordParser
import mock

class TestUtils(object):
    def build_test_context(self):
        with open('test_config.json', 'w') as test_config:
            test_config.write("""{
                    "Subreddits": ["Romania", "HootSuite", "aoe2", "Python", "Docker"],
                    "AggregationIntervalInSeconds": 60,
                    "ClientId" : "a",
                    "ClientSecret" : "b",
                    "Password" : "c", 
                    "UserAgent" : "d",
                    "Username" : "e",
                    "DatabaseUri": "mongodb://localhost:27017/",
                    "DatabaseName": "reddit"
                    }""")
        return AggregatorContext.AggregatorContext(TestClientBuilder(), "test_config.json")

class Expando(object):
    pass

class TestClientBuilder(object):
    def __subreddit(self, p):
        return {}
    def build_reddit_client(self, config):
        reddit_client_expando = Expando()
        reddit_client_expando.subreddit = self.__subreddit
        return reddit_client_expando

    def build_db_client(self, config):
        return {
            "Romania" : {},
            "Hootsuite" : {}
        }

class AggregatorContextTests(unittest.TestCase):
    def tearDown(self):
        os.remove("test_config.json")

    def test_config_parsed(self):
        # Arrange
        test_utils = TestUtils()

        # Act
        self.context = test_utils.build_test_context()

        # Assert
        self.assertEqual(5, len(self.context.config["Subreddits"]))
        self.assertItemsEqual(["Romania", "HootSuite", "aoe2", "Python", "Docker"], self.context.config["Subreddits"])
        self.assertEqual(60, self.context.config["AggregationIntervalInSeconds"])
        self.assertEqual("a", self.context.config["ClientId"])
        self.assertEqual("b", self.context.config["ClientSecret"])
        self.assertEqual("c", self.context.config["Password"])
        self.assertEqual("d", self.context.config["UserAgent"])
        self.assertEqual("e", self.context.config["Username"])
        self.assertEqual("mongodb://localhost:27017/", self.context.config["DatabaseUri"])
        self.assertEqual("reddit", self.context.config["DatabaseName"])

class CommentEntryBuilderTests(unittest.TestCase):
    def setUp(self):
        self.keyword_parser = KeywordParser.KeywordParser()
        self.builder = CommentEntryBuilder.CommentEntryBuilder(self.keyword_parser)

    def parse_keywords_mock(self, k):
        return ["a", "b"]

    def test_comment_from_anonymous_author_parsed(self):
        # Arrange
        # Mocking KeywordParser.parse_keywords to test the CommentEntryBuilder in isolation
        self.keyword_parser.parse_keywords = self.parse_keywords_mock

        # Act
        comment = self.builder.build_comment("1", "2", None, "b", "c")

        # Assert
        self.assertEqual("1", comment["_id"])
        self.assertEqual(int(2), comment["timestamp"])
        self.assertEqual("", comment["author"])
        self.assertEqual("b", comment["text"])
        self.assertEqual("c", comment["subreddit"])
        self.assertEqual(["a", "b"], comment["keywords"])

    def test_comment_from_existing_author_parsed(self):
        # Arrange
        # Mocking KeywordParser.parse_keywords to test the CommentEntryBuilder in isolation
        self.keyword_parser.parse_keywords = self.parse_keywords_mock
        author = Expando()
        author.name = "x"

        # Act
        comment = self.builder.build_comment("1", "2", author, "b", "c")

        # Assert
        self.assertEqual("1", comment["_id"])
        self.assertEqual(int(2), comment["timestamp"])
        self.assertEqual("x", comment["author"])
        self.assertEqual("b", comment["text"])
        self.assertEqual("c", comment["subreddit"])
        self.assertEqual(["a", "b"], comment["keywords"])

class KeywordParserTests(unittest.TestCase):
    def test_parsing_removes_non_words(self):
        # Arrange
        keyword_parser = KeywordParser.KeywordParser()

        # Act
        keywords = keyword_parser.parse_keywords("!!this is a test1!$")

        # Assert
        self.assertItemsEqual(["this", "is", "a", "test1"], keywords)

class DataAggregationEngineComponentTests(unittest.TestCase):
    # Normally, I'd mock out both the Reddit layer and the database layer and test the aggregation in isolation too
    # However, for our scenario this would be too costly to code, so we'll just test if the aggregation does not throw
    # errors should be caught

    def no_op(self):
        pass

    def test_data_aggregation_engine_is_creatable(self):
        # Arrange
        test_utils = TestUtils()
        context = test_utils.build_test_context()

        # Act
        self.engine = DataAggregationEngine.DataAggregationEngine(
            TestClientBuilder(),
            CommentEntryBuilder.CommentEntryBuilder(KeywordParser.KeywordParser()),
            context)
        self.engine.__add_indexes_if_not_present = self.no_op
        self.engine.perform_aggregation()

        # No assert needed, as we just expect this not to throw
