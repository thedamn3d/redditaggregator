import praw
import pymongo

from itertools import groupby
from praw.models import MoreComments
from pymongo.errors import BulkWriteError

class DataAggregationEngine(object):
    def __init__(self, client_builder, comment_entry_builder, context):
        self.client_builder = client_builder
        self.logger = context.logger
        self.config = context.config
        self.comment_entry_builder = comment_entry_builder

    def perform_aggregation(self):
        try:
            self.logger.info("Aggregation starting, pulling data...")
            self.logger.info("Connecting to " + self.config["DatabaseUri"])

            # For a very small polling interval, we might want to keep the connection open,
            # but for our case we can just re-connect every time
            with self.client_builder.build_db_client(self.config) as client:
                database = client[self.config["DatabaseName"]]

                self.logger.info("Connected to database ." + self.config["DatabaseName"])
                with self.client_builder.build_reddit_client(self.config) as reddit_api:
                    self.__perform_aggregation_core(database, reddit_api)

            self.logger.info("Aggregation round finished.")

        except Exception, e:
            # On failure, we will fail the entire aggregation round and let the outer loop try again
            # Skip logging for duplicate key errors in our case, since, for perf reasons,
            # we are ok with having them
            self.logger.error('An error occurred while performing the aggregation: '+ str(e))

    def __perform_aggregation_core(self, database, reddit_api):
        subreddits = reddit_api.subreddit('+'.join(self.config["Subreddits"]))
        # Hard-coding limit to 100 for the purpose of this aggregator. In a production-grade
        # system, we'd want some way of figuring out what was the last one you processed,
        # or use a much bigger number.
        submissions = subreddits.new(limit=100)

        self.logger.info("Fetching comments...")
        comments_list = self.__build_comments_list(submissions)

        # After we have finished processing all submissions, we'll bulk-add the entries
        # per subreddit.

        # Add the index if this is the first time that we access this collection
        # For many subreddits (and/or tons of comments) we should page everything

        mapper = lambda x: x["subreddit"]
        for key, subreddit_comment_group in groupby(sorted(comments_list, key=mapper), mapper):
            submission_comments_collection = database[key]

            self.__add_indexes_if_not_present(submission_comments_collection)

            # If there are matched comments, MongoDB will match them at the database layer,
            # so we can just add all of them, without worrying about duplicates.
            # It's probably much cheaper to do so than to query if they exist.
            self.logger.info("Processing comments from subreddit " + key)

            try:
                bulk = submission_comments_collection.initialize_unordered_bulk_op()
                for comment in subreddit_comment_group:
                    bulk.insert(comment)
                bulk.execute()
            except BulkWriteError, bwe:
                # Skip 11000 duplicate key errors while adding the comments, as these
                # are expected to happen and the alternative would probably be less performant
                error_code_mapper = lambda e: e["code"]
                sorted_error_codes = sorted(map(error_code_mapper, bwe.details["writeErrors"]), key=lambda x: x)
                error_codes = list(groupby(sorted_error_codes))
                if len(error_codes) == 1 and error_codes[0] == 11000:
                    continue
                self.logger.error('An error occurred while performing the aggregation: '+ str(bwe))

    def __build_comments_list(self, submissions):
        comments_list = list()

        for submission in submissions:
            # The submission is to be treated as just another comment,
            # with the title parsed for keywords instead of the body

            comments_list.append(self.comment_entry_builder.build_comment(
                submission.id,
                submission.created_utc,
                submission.author,
                submission.title,
                submission.subreddit.display_name))

            # Replace all the MoreComments placeholders
            submission.comments.replace_more(limit=0)

            # Flatten the list, since we don't care about hierarchy
            flattened_comment_list = submission.comments.list()

            # Process all comments
            for comment in flattened_comment_list:
                if isinstance(comment, MoreComments):
                    continue
                comments_list.append(self.comment_entry_builder.build_comment(
                    comment.id,
                    comment.created_utc,
                    comment.author,
                    comment.body,
                    submission.subreddit.display_name))

        return comments_list

    def __add_indexes_if_not_present(self, submission_comments_collection):
        if submission_comments_collection.find_one() is None:
            submission_comments_collection.create_index(
                [('keywords', pymongo.ASCENDING)],
                unique=False)
            submission_comments_collection.create_index(
                [('timestamp', pymongo.ASCENDING)],
                unique=False)
