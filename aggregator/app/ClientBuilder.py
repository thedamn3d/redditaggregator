import praw
from pymongo import MongoClient

class ClientBuilder(object):
    def build_reddit_client(self, config):
        return praw.Reddit(
            client_id=config["ClientId"],
            client_secret=config["ClientSecret"],
            password=config["Password"],
            user_agent=config["UserAgent"],
            username=config["Username"])

    def build_db_client(self, config):
        return MongoClient(config["DatabaseUri"])
