import json
import logging
from pymongo import MongoClient

class AggregatorContext(object):
    def __init__(self, clientBuilder, config_path):
        self.client_builder = clientBuilder
        self.config_path = config_path
        self.logger = self.__create_logger()
        self.config = self.__read_configuration()

    def clean_db(self):
        # Since one of the indications is that it's ok to start fresh every time,
        # we'll just delete the database if it already exists
        with self.client_builder.build_db_client(self.config) as client:
            db_name = self.config["DatabaseName"]

            if db_name in client.database_names():
                client.drop_database(db_name)

    def __create_logger(self):
        logger = logging.getLogger('aggregator')
        # In production you'd use a rotating file handler (and upload to a telemetry parser)
        log_handler = logging.FileHandler('aggregator.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        log_handler.setFormatter(formatter)
        logger.addHandler(log_handler)
        logger.setLevel(logging.INFO)
        return logger

    def __read_configuration(self):
        self.logger.info("Reading the configuration...")

        with open(self.config_path) as data_file:
            config = json.load(data_file)

        return config
