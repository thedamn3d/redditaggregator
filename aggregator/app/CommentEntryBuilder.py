class CommentEntryBuilder(object):
    def __init__(self, keyword_parser):
        self.keyword_parser = keyword_parser

    def build_comment(self, identifier, timestamp, author, text, subreddit):
        author_name = ""

        if author is not None:
            author_name = author.name

        keywords = self.keyword_parser.parse_keywords(text)
        return {
            "_id": identifier,
            "timestamp" : int(timestamp),
            "author": author_name,
            "text": text,
            "keywords": keywords,
            "subreddit": subreddit
        }
        