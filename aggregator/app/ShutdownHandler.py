import signal

class ShutdownHandler(object):
    should_stop = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.__exit_gracefully)
        signal.signal(signal.SIGTERM, self.__exit_gracefully)

    def __exit_gracefully(self, signum, frame):
        self.should_stop = True
        