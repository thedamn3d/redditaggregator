import re

class KeywordParser(object):
    pattern = re.compile(r'[\W_]+')

    def parse_keywords(self, text):
        return self.pattern.sub(" ", text).split()

