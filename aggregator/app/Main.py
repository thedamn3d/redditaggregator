"""Architectural discussion:

I'm going to go with the simple model of an infrequent pull (usually the number of API calls
is limited by the API anyway)

Hence, we only want to connect whenever we want to pull the data
(defaulted the polling interval to 60s)

Of course, the best case would be to have it directly pushed to us.

Also, in a production-grade Reddit aggregator, I would scale out the number of fetchers,
and do some sort of distribution (the simplest would be subreddits,
but for large subreddits that might not be enough).

However, here I'll just go with a vertical architecture, as described in the exercise, and,
as for the code, it is structured into classes and using dependency injection wherever possible,
to enable high testability throughout the codebase.
"""

#!/usr/bin/python
import time

from AggregatorContext import AggregatorContext
from CommentEntryBuilder import CommentEntryBuilder
from DataAggregationEngine import DataAggregationEngine
from ClientBuilder import ClientBuilder
from KeywordParser import KeywordParser
from ShutdownHandler import ShutdownHandler

if __name__ == '__main__':
    # Initialization
    # Injecting class dependencies into the constructor for testability
    SHUTDOWNHANDLER = ShutdownHandler()
    CONTEXT = AggregatorContext(ClientBuilder(), "config.json")
    DATAGGREGATIONENGINE = DataAggregationEngine(ClientBuilder(), CommentEntryBuilder(KeywordParser()), CONTEXT)
    CONTEXT.clean_db()

    # Loop until termination
    print "Started. Logging to file aggregator.log"

    while True:
        DATAGGREGATIONENGINE.perform_aggregation()
        time.sleep(CONTEXT.config["AggregationIntervalInSeconds"])

        if SHUTDOWNHANDLER.should_stop:
            break

    CONTEXT.logger.info("Termination signal received, stopping the process.")
