#!flask/bin/python
from app import app
from app import AppContext
from pymongo import MongoClient

if __name__ == '__main__':
    app.context = AppContext("config.json")
    app.flask.run(host = '0.0.0.0')
