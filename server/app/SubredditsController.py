import pymongo
import json

from pymongo import MongoClient
from flask import abort, request, Response
from app import app

def fetch_query_result(subreddit, query):
    context = app.context
    with MongoClient(context.config["DatabaseUri"]) as client:
        database = client[context.config["DatabaseName"]]
        subreddit_collection = database[subreddit]

        query_result = list()
        for comment in subreddit_collection.find(query).sort("timestamp", pymongo.DESCENDING):
            query_result.append(comment)
        return query_result

@app.flask.route('/items/')
def index():
    subreddit = request.args.get('subreddit')
    t_from = request.args.get('from')
    t_to = request.args.get('to')
    keyword = request.args.get('keyword')

    if subreddit is None or t_from is None or t_to is None or not t_from.isdigit() or not t_to.isdigit():
        abort(400)

    query = {"timestamp": {"$lte": int(t_to), "$gte": int(t_from)}}

    if keyword is not None:
        query["keywords"] = keyword

    query_result = fetch_query_result(subreddit, query)
    try:
        return Response(json.dumps(query_result), mimetype='application/json')
    except Exception, e:
        abort(500)
