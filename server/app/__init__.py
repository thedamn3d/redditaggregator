from AppContext import AppContext
from flask import Flask

class WebApp(object):
    def __init__(self, flask):
        self.flask = flask

flask_container = Flask("RedditAggregatorApi")
app = WebApp(flask_container)

from app import SubredditsController