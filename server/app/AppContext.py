import json

class AppContext(object):
    def __init__(self, config_path):
        self.config_path = config_path
        self.config = self.__read_configuration()

    def __read_configuration(self):
        with open(self.config_path) as data_file:
            config = json.load(data_file)

        return config
