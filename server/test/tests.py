import os
import unittest
from app import AppContext
import mock

class TestUtils(object):
    def build_test_context(self):
        with open('test_config.json', 'w') as test_config:
            test_config.write("""{
                    "Subreddits": ["Romania", "HootSuite", "aoe2", "Python", "Docker"],
                    "AggregationIntervalInSeconds": 60,
                    "ClientId" : "a",
                    "ClientSecret" : "b",
                    "Password" : "c", 
                    "UserAgent" : "d",
                    "Username" : "e",
                    "DatabaseUri": "mongodb://localhost:27017/",
                    "DatabaseName": "reddit"
                    }""")
        return AppContext("test_config.json")

class AppContextTests(unittest.TestCase):
    def tearDown(self):
        os.remove("test_config.json")

    def test_config_parsed(self):
        # Arrange
        test_utils = TestUtils()

        # Act
        self.context = test_utils.build_test_context()

        # Assert
        self.assertItemsEqual(["Romania", "HootSuite", "aoe2", "Python", "Docker"], self.context.config["Subreddits"])
        self.assertEqual(60, self.context.config["AggregationIntervalInSeconds"])
        self.assertEqual("a", self.context.config["ClientId"])
        self.assertEqual("b", self.context.config["ClientSecret"])
        self.assertEqual("c", self.context.config["Password"])
        self.assertEqual("d", self.context.config["UserAgent"])
        self.assertEqual("e", self.context.config["Username"])
        self.assertEqual("mongodb://localhost:27017/", self.context.config["DatabaseUri"])
        self.assertEqual("reddit", self.context.config["DatabaseName"])

